var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
var https = require('https');
var fs = require('fs');
var path = require('path');

var sslOptions = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem'),
  passphrase: 'techu'
};

app.use(express.static(__dirname + '/build/default'));

https.createServer(sslOptions, app).listen(port)
//app.listen(port);

console.log('TechuBank started on port ' + port);

app.get('/', function(req, res) {
  res.sendFile("index.html", {root: '.'});
});
