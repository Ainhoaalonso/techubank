'use strict';
var jwt = require('jsonwebtoken');
var mlabClientsController = require('../controllers/mlabClientsController');
var config = require('../../config');
const { Pool} = require('pg');
const bcrypt = require('bcrypt');
const pgConnectionString = process.env.TECHUBANK_PG_CONN|| config.pgConnectionString;
const pool = new Pool({
  connectionString: pgConnectionString,
});

exports.signUp = function(req, res) {
  console.log("pgUsersController.signUp");
  var clientId = req.body.id;
  var passwordHash = bcrypt.hashSync(req.body.password, 10);
  const query = {
    text: 'INSERT INTO users (username, password, id) VALUES($1, $2, $3) RETURNING *',
    values: [req.body.username,passwordHash,clientId],
  };

  pool.query(query, (err, result) => {
    if (err) {
      console.log(err);
      res.status(500).send({result:'ERR',detail:err.detail});
    } else {
      var newClient = {
        clientId:clientId,
        accounts:[{
          accountId:'ES99'+ clientId.substring(0, clientId.length-1)+'0',
          movs:[]
        }]
      };
      mlabClientsController.insertClient(newClient, function(err,body) {
        if(err)  {
          console.log(err);
          res.status(200).send({result:'OK, no mlab'});
        }  else {
          res.status(200).send({result:'OK'});
        }

      });
    }
  });
};


exports.login = function(req, res) {
  console.log("pgUsersController.login"+req.body.username);
  var response = {};
  const query = {
  text: 'SELECT * FROM users WHERE username=$1',
  values: [req.body.username],
  }
  pool.query(query, (err, result) => {
    if (err) {
      res.status(500).send({result:'ERR',detail:err.detail});

    } else {
      if (result.rowCount > 0){
        if(bcrypt.compareSync(req.body.password, result.rows[0].password)) {
          // create a token
          var token = jwt.sign({ id: result.rows[0].id }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
          });
          res.status(200).send({
            result:'OK',
            user:result.rows[0].username,
            id:result.rows[0].id,
            token:token}
          );
        } else {
          res.status(500).send({result:'ERR',detail:'Password is wrong'});
        }
      } else {
        res.status(500).send({result:'ERR',detail:'User unknown'});
      }
    }
    });
};
