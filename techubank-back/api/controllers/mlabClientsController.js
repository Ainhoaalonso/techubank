'use strict';

//variable para poder usar el request-json
var requestJson = require('request-json');
var config = require('../../config');

var urlApiKey=config.mlabApiKey;
var clientMlab = requestJson.createClient(config.mlabUrl);

exports.listAllClients = function(req, res) {
  clientMlab.get('?' + urlApiKey, function(err, resM, body) {
    if(err) {
        res.status(500).send({ result: 'ERR', detail: err });
    }
    else {
      res.status(200).send(body);
    }
  });
};

exports.insertClient = function(newClient,callback){
  clientMlab.post('?' + urlApiKey, newClient, function(err, resM, body) {
    if(err)  {
      console.log(err);
      callback(err,undefined);
    }  else {
      callback(undefined,body);
    }
  });
};

exports.createClient = function(req, res) {
  var tokenResult=verifyToken(req, res);
  exports.insertClient(req.body, function(err,body) {
    if(err)  {
      console.log(err);
      res.status(500).send({ result: 'ERR', detail: err });
    }  else {
      res.status(200).send({ result: 'OK', detail: body });
    }
  });
};

function updateClient(clientId,updatedClient,callback){
  clientMlab.put('?q={"clientId":"'+clientId+'"}' + '&' + urlApiKey,updatedClient, function(err, resM, body) {
    if(err)  {
      console.log(err);
      callback(err,undefined);
    }  else {
      callback(undefined,body);
    }
  });
};

exports.modifyClient = function(req, res) {
  updateClient(req.params.clientId,req.body, function(err,body) {
    if(err)  {
      console.log(err);
      res.status(500).send({ result: 'ERR', detail: err });
    }  else {
      res.status(200).send(body);
    }
  });
};

function findClient(clientId,callback){
  clientMlab.get('?q={"clientId":"'+ clientId +'"}' + '&' + urlApiKey, function(err, resM, body) {
    if(err)  {
      console.log(err);
      callback(err,undefined);
    }  else {
      callback(undefined,body);
    }
  });
};

exports.getClient = function(req, res) {
  findClient(req.params.clientId, function(err,body) {
    if(err)  {
      console.log(err);
      res.status(500).send({ result: 'ERR', detail: err });
    }  else {
      res.status(200).send({ result: 'OK', detail: body });
    }
  });

};

function addMovToPosition(mov, positionList){
  let amount =Math.abs(mov.amount);
  positionList.total= positionList.total + amount;
  if (positionList.categories[mov.category]){
    positionList.categories[mov.category]=positionList.categories[mov.category] + amount;
  } else {
    positionList.categories[mov.category]=amount;
  }
}

exports.getClientPosition = function(req, res) {
  clientMlab.get('?q={"clientId":"'+req.params.clientId+'"}&f={"accounts.movs.address": 0,"accounts.movs.item":0}' + '&' + urlApiKey, function(err, resM, body) {
    if(err)  {
      console.log(err);
      res.status(500).send({ result: 'ERR', detail: err });
    }  else  {
      var position = {
        expenses:{
          total:0,
          categories:{}},
        incomes:{
          total:0,
          categories:{}}
      };

      for (let account of body[0].accounts) {
        for (let mov of account.movs) {
          const date = new Date(mov.date.$date);
          const year = date.getFullYear();
          const month = 1+date.getMonth();

          if (year == req.params.year && month ==req.params.month) {
            if (mov.amount < 0){
              addMovToPosition(mov, position.expenses);
            } else {
              addMovToPosition(mov, position.incomes);
            }
          }
        }
      }
      res.status(200).send({ result: 'OK', detail: position });
    }
  });
};

exports.createMov= function(req, res) {
  findClient(req.params.clientId, function(err,body) {
    if(err)  {
      console.log(err);
        res.status(500).send({ result: 'ERR', detail: err });
    }  else {
      if (body.length > 0){
        let account;
        for (account of body[0].accounts) {
          if (req.params.account ==account.accountId){
            break;
          }
        }

        if (account){
          account.movs.push({
            date: {
               $date: new Date(req.body.date).toISOString()
           },
           item: req.body.item,
           amount: req.body.amount,
           category: req.body.category,
           address: req.body.address?req.body.address:' '
          });
          updateClient(req.params.clientId,body, function(err,body) {
            if(err)  {
              console.log(err);
              res.status(500).send({ result: 'ERR', detail: err });
            }  else {
              res.status(200).send({result:"OK", detail:''});
            }
          });

        }else {
          res.status(500).send({ result: 'ERR', detail: 'No account was found' });
        }
      }else {
        res.status(500).send({ result: 'ERR', detail: 'No client was found' });
      }
    }
  });
};

exports.createAccount= function(req, res) {
  let clientId = req.params.clientId;
  findClient(clientId, function(err,body) {
    if(err)  {
      console.log(err);
      res.status(500).send({ result: 'ERR', detail: err });
    }  else {
      if (body.length > 0){
        var numAccounts=0;
        let account;
        for (account of body[0].accounts) {
          numAccounts = numAccounts+1;
        }
        let newAccountId = 'ES99'+ clientId.substring(0, clientId.length-1)+numAccounts;
        body[0].accounts.push({
          accountId:newAccountId,
          movs:[]
        });

        updateClient(clientId,body, function(err,body) {
          if(err)  {
            console.log(err);
            res.status(500).send({ result: 'ERR', detail: err });
          }  else {
            res.status(200).send({
              result:"OK",
              detail:{accountId:newAccountId}
          });
          }
        });
      }else {
        res.status(500).send({ result: 'ERR', detail: 'No client was found' });
      }
    }
  });
};
