'use strict';
module.exports = function(app) {
  var mlabClientsController = require('../controllers/mlabClientsController');
  var pgUsersController = require('../controllers/pgUsersController');
  var verifyToken = require('../controllers/VerifyToken');

  app.use(function (req, res, next) {
    console.log('Request URL:', req.originalUrl)
    next()
  }, function (req, res, next) {
    console.log('Request Type:', req.method)
    next()
  });
  app.use('/clients',verifyToken);
  app.route('/login')
    .post(pgUsersController.login);

  app.route('/signup')
    .post(pgUsersController.signUp);

  app.route('/clients')
    .get(mlabClientsController.listAllClients)
    .post(mlabClientsController.createClient);

  app.route('/clients/:clientId')
    .get(mlabClientsController.getClient)
    .put(mlabClientsController.modifyClient);


  app.route('/clients/:clientId/position/:year/:month')
    .get(mlabClientsController.getClientPosition);

  app.route('/clients/:clientId/accounts')
    .post(mlabClientsController.createAccount);

  app.route('/clients/:clientId/accounts/:account/movements')
    .post(mlabClientsController.createMov);
};
