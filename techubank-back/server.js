var express = require('express'),
  app = express(),
  port = process.env.PORT || 8443;
var https = require('https');
var fs = require('fs');
var path = require('path');
var cors = require('cors')
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
var routes = require('./api/routes/techuRoutes'); //importing route

var sslOptions = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem'),
  passphrase: 'techu'
};

app.use(cors());
app.use(bodyParser.json()); // for parsing application/json

routes(app);//register the route
https.createServer(sslOptions, app).listen(port)

console.log('TechuBank server started on: ' + port);

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})
